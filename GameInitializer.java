import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

 
public class GameInitializer extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        GameEngine.mainStage = primaryStage;
        StackPane layout1 = new StackPane();
        VBox vbox = new VBox();
        VBox root = new VBox();
        Scene playerNamesScene = new Scene(layout1, Config.windowSize, Config.windowSize);
        Scene mainScene = new Scene(root, Config.windowSize, Config.windowSize);
        Button btn = new Button("1 player");
        Button btn2 = new Button("2 players");
        Button btn3 = new Button("Play");
        TextField player1Name = new TextField();
        TextField player2Name = new TextField();
        GameEngine.mainStage.setTitle("Tic tac toe");
        btn.setOnAction((ActionEvent event)->{
                GameEngine.mainStage.setScene(playerNamesScene);
                GameEngine.numberOfPlayers = 1;
            
        }); 
        btn2.setOnAction((ActionEvent event)->{
                GameEngine.mainStage.setScene(playerNamesScene);
                GameEngine.numberOfPlayers = 2;
                vbox.getChildren().add(1,player2Name);
        }); 
        
        btn3.setOnAction((ActionEvent event)->{ 
                String p1Name = player1Name.getText();
                if(!p1Name.trim().isEmpty() && p1Name!=null){
                   System.out.println(p1Name);
                   Player p1 = new Player(p1Name,'o',false);
                   GameEngine.addPlayer(p1);
                    if(GameEngine.numberOfPlayers == 1){
                        Player p2 = new Player("AI",'x',true);
                        GameEngine.addPlayer(p2);
                        Game.Init();
                    };
                } else{
                    System.out.println("p2Name");
                    Popup.create(layout1, "Name of player 1 can't be empty and can't start with blank space",4000.0);
                }
                if(GameEngine.numberOfPlayers == 2){
                    String p2Name = player2Name.getText();
                    if(!p2Name.trim().isEmpty() && p2Name!=null){
                       System.out.println(p2Name);
                       Player p2 = new Player(p2Name,'x',false);
                       GameEngine.addPlayer(p2);
                       Game.Init();
                    //    System.exit(1);
                    } else {
                        System.out.println("p2Name");
                        Popup.create(layout1, "Name of player 2 can't be empty and can't start with blank space",4000.0);
                    }
                }
            });
        player1Name.setMaxWidth(300);
        player2Name.setMaxWidth(300);
        player1Name.setPromptText("Player one name");
        player2Name.setPromptText("Player two name");
        // player1Name.setTranslateY(-100);
        // player2Name.setTranslateY(-50);
        GameEngine.mainStage.setTitle("Tic tac toe");
        layout1.getChildren().addAll(vbox);
        vbox.getChildren().addAll(player1Name,btn3);
        vbox.setPadding(new Insets(10));
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.getChildren().addAll(btn,btn2);
        root.setPadding(new Insets(10));
        root.setAlignment(Pos.CENTER);
        root.setSpacing(10);
        root.getChildren().addAll(btn,btn2);
        GameEngine.mainStage.setScene(mainScene);
        GameEngine.mainStage.show();
    }
}