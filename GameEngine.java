import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.stage.Stage;
import javafx.util.Duration;

class GameEngine{
    private static ArrayList<Player> playerList = new ArrayList<>();
    public static int numberOfPlayers;
    public static Stage mainStage;
    private static int move = 0;
    private static boolean beginingPlayerId = false ;
    private static boolean activePlayerId = false ;
    public static void addPlayer(Player p1){
        playerList.add(p1);
    }
    public static void changeGameBegginer(){
        beginingPlayerId=!beginingPlayerId;
    }
    public static void beginGame(){
        activePlayerId = beginingPlayerId;
        if(!getMovingPlayer().isAi){
            Popup.create(Game.mainGameStack, getMovingPlayer().getName()+" turn",1000.0);
        } else{
            makeAiTurn(9-move,activePlayerId);
        }
    }
    public static void nextMove(){
        move++;
        if(checkWinningCondition()){
            System.out.println("WON: "+ getMovingPlayer().getName());
            PostGameScreen.init(GameEngine.playerList.indexOf(getMovingPlayer()));
        }
        if(move==9){
            System.out.println("TIE");
            PostGameScreen.init(-1);
        }
        activePlayerId=!activePlayerId;
        if(!getMovingPlayer().isAi){
            Popup.create(Game.mainGameStack, getMovingPlayer().getName()+" turn",1000.0);
        } else{
            makeAiTurn(9-move,activePlayerId);
        }
    }
    public static Player getMovingPlayer(){
        return playerList.get(activePlayerId?1:0);
    }
    private static Boolean checkWinningCondition(){
        // Game.tileList.forEach((tile)->{
        //     if(tile.getFillType()!=' ')
        //     System.out.println(tile.getId()+":"+tile.getFillType());
        // });
        for(int i=0;i<3;i++){
            if(Game.tileList.get(i*3).getFillType() == Game.tileList.get(i*3+1).getFillType() &&  Game.tileList.get(i*3+1).getFillType() == Game.tileList.get(i*3+2).getFillType() && Game.tileList.get(i*3+1).getFillType()!=' ')
                return true;
            if(Game.tileList.get(i).getFillType() == Game.tileList.get(i+3).getFillType() &&  Game.tileList.get(i+3).getFillType() == Game.tileList.get(i+6).getFillType() && Game.tileList.get(i+3).getFillType()!=' ')
                return true;
            }
            if(Game.tileList.get(0).getFillType() == Game.tileList.get(4).getFillType() &&  Game.tileList.get(4).getFillType() == Game.tileList.get(8).getFillType() && Game.tileList.get(4).getFillType()!=' ')
                return true;
            if(Game.tileList.get(2).getFillType() == Game.tileList.get(4).getFillType() &&  Game.tileList.get(4).getFillType() == Game.tileList.get(6).getFillType() && Game.tileList.get(4).getFillType()!=' ')
                return true;
        return false;
    }
    public static void clearGame(){
        changeGameBegginer();
        move = 0;
        Game.tileList.clear();
    }
    public static Player getPlayerById(Integer id){
        return playerList.get(id);
    }
    private static void makeAiTurn(int movesLeft, boolean playerBool){
        if(!Game.tileList.isEmpty()){
            // Random rand = new Random();
            // do{
                //     int id = rand.nextInt(8);
                //     Tile tile = Game.tileList.get(id);
                //     if(tile.getFillType()==' '){
                    //         tile.makeMove();
                    //         return;
                    //     }
                    // }while(true);
            if(movesLeft == 0) return;
            System.out.println(movesLeft);
            Player active = playerList.get(playerBool?1:0);
            for(int i=0;i<1;i++){
                boolean ghostMoveDone = false;
                int id = 0;
                do{
                Tile tile = Game.tileList.get(id);
                    if(tile.getFillType()==' '){
                        tile.makeGhostMove(active);
                        ghostMoveDone = true;
                    }
                    id++;
                }while(!ghostMoveDone);
                Timeline timeline = new Timeline(new KeyFrame(
                    Duration.millis(1000),
                    ae->makeAiTurn(movesLeft-1, !playerBool)
                ));
                timeline.play();
            }

        }
    }
}