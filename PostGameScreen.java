import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Font;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
class PostGameScreen{

    public static void init(Integer id){
        Text winningText = new Text("Tie");
        if(id!=-1){
            Player winningPlayer = GameEngine.getPlayerById(id);
            winningPlayer.addPoint();
            winningText.setText(winningPlayer.getName().toUpperCase()+" HAS WON");
        }
        winningText.setFont(Font.font ("Verdana", 30));
        Text scores = new Text("SCORE:");
        scores.setFont(Font.font ("Verdana", 25));
        Text scoreLabel = new Text(GameEngine.getPlayerById(0).getName() + " " + GameEngine.getPlayerById(0).getPoints() + " : " +  GameEngine.getPlayerById(1).getPoints() + " " + GameEngine.getPlayerById(1).getName());
        GameEngine.clearGame();
        VBox vbox = new VBox();
        Scene postGameScene = new Scene(vbox, Config.windowSize, Config.windowSize);
        Button btn = new Button("Play again");
        Button btn2 = new Button("Exit");
        GameEngine.mainStage.setScene(postGameScene);
        btn.setOnAction((ActionEvent event)->{
            Game.Init();        
        }); 
        btn2.setOnAction((ActionEvent event)->{
            System.exit(1);
        }); 
        vbox.setPadding(new Insets(10));
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.getChildren().addAll(winningText,scores,scoreLabel,btn,btn2);
    }
}