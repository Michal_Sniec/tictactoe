import java.util.ArrayList;

import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

class Game{
    public static ArrayList<Tile> tileList = new ArrayList<>();
    public static StackPane mainGameStack = new StackPane();
    public static Scene gameScene = null;
    public static void Init(){
        initBoard();
        GameEngine.beginGame();

    }
    private static void initBoard(){
        FlowPane layout = new FlowPane();
        mainGameStack.getChildren().add(layout);
        if(gameScene == null){
            gameScene = new Scene(mainGameStack,Config.windowSize, Config.windowSize);
        }
        GameEngine.mainStage.setScene(gameScene);
        layout.setStyle("-fx-background-color: #000000;");
        layout.setPrefWrapLength(Config.windowSize);
        layout.setVgap(Config.laneWidth);
        layout.setHgap(Config.laneWidth);
        for(int i=0;i<9;i++){
            Tile temp = new Tile(i);
            layout.getChildren().add(temp.getBody());
            tileList.add(temp);
        }
    }
}