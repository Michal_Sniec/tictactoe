class Player{
    private String name;
    private int points;
    public Character sign;
    public Boolean isAi;
    Player(String name,Character sign, Boolean isAi){
        this.points = 0;
        this.name = name;
        this.sign = sign;
        this.isAi = isAi;
    }
    public String getName(){
        return this.name;
    }
    public void addPoint(){
        points++;
    }
    public Integer getPoints(){
        return points;
    }
}