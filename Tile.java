import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

class Tile{
    private int id;
    private StackPane body;
    private Character fillType;
    Tile(int id){
        StackPane pane = new StackPane();
        pane.setMaxSize(Config.fieldSize, Config.fieldSize);
        Rectangle rect = new Rectangle(Config.fieldSize, Config.fieldSize);
        this.id = id;
        rect.setFill(Color.WHITE);
        pane.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if(getFillType()!=' ') return;
                makeMove();
            }
        });
        pane.getChildren().addAll(rect);
        this.body = pane;
        this.fillType = ' ';
    }
    /**
     * @return the body
     */
    public StackPane getBody() {
        return body;
    }
    private void setFillType(){
        fillType = GameEngine.getMovingPlayer().sign;
    }
    public Character getFillType(){
        return fillType;
    }
    public Integer getId(){
        return this.id;
    }
    public void makeMove(){
        setFillType();
        Text fill = new Text(getFillType().toString());
        fill.setFont(Font.font ("Verdana", Config.fieldSize/2));
        this.body.getChildren().addAll(fill);
        GameEngine.nextMove();
    }
    public void makeGhostMove(Player p){
        fillType = p.sign;
        Text fill = new Text(getFillType().toString());
        fill.setFont(Font.font ("Verdana", Config.fieldSize/6));
        this.body.getChildren().addAll(fill);
    }
}