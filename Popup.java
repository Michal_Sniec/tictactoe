import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
class Popup{
    static boolean isActive = false;
    public static void create(StackPane scene,String text, Double fullDuration){
        if (isActive) return;
        isActive = !isActive;
        Text xd = new Text(text);
        StackPane stack = new StackPane();
        stack.setMaxSize(Config.windowSize, 50);
        Rectangle rectBasicTimeline = new Rectangle(Config.windowSize,50);
        stack.getChildren().addAll(rectBasicTimeline,xd);
        stack.setTranslateY(-Config.windowSize /2 - 25);
        rectBasicTimeline.setFill(Color.rgb(255, 0,0, 0.7));
        scene.getChildren().addAll(stack);
        Timeline timeline = new Timeline();
        
        Duration time = new Duration(fullDuration / 8);
        Duration time2 = new Duration(fullDuration * 6/8);
        Duration time3 = new Duration(fullDuration * 7/8);
        
        
        KeyValue kv = new KeyValue(stack.translateYProperty(), -Config.windowSize /2 + 25);
        KeyFrame kf = new KeyFrame(time, kv);
        KeyFrame kf2 = new KeyFrame(time2, kv);
        KeyValue kv3 = new KeyValue(stack.translateYProperty(), -Config.windowSize /2 - 25);
        KeyFrame kf3 = new KeyFrame(time3, kv3);
        timeline.getKeyFrames().addAll(kf,kf2,kf3);
        timeline.play();
        timeline.setOnFinished((event)->{
            isActive = !isActive;
            scene.getChildren().remove(stack);
        });
    }
}