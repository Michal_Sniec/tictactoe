class Config {
    
    public static int fieldSize = 150;
    public static int laneWidth = 10;
    public static int windowSize = Config.fieldSize * 3 + Config.laneWidth * 2;

}